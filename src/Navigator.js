import React, {useEffect} from 'react';
import {Pressable, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import ChatListScreen from './scenes/chatList/ChatListScreen';
import ChatScreen from './scenes/chat/ChatScreen';
import {matrix} from 'rn-matrix';
import ChatDetailsScreen from './scenes/chatDetails/ChatDetailsScreen';

const homeserverUrl = 'https://matrix.ditto.chat';
const user = '@test:ditto.chat';
const accessToken =
  'MDAxOGxvY2F0aW9uIGRpdHRvLmNoYXQKMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDIzY2lkIHVzZXJfaWQgPSBAdGVzdDpkaXR0by5jaGF0CjAwMTZjaWQgdHlwZSA9IGFjY2VzcwowMDIxY2lkIG5vbmNlID0gLVZuX3RZVjJpRk80V2QsKwowMDJmc2lnbmF0dXJlIGYlwrKiStuijF4uaQ9KJStxRDueNHpAT3b74ZaZI-n_Cg';
const deviceId = 'EBIPBHNMDO';

const Stack = createStackNavigator();

function Navigator() {
  useEffect(() => {
    matrix.createClient(homeserverUrl, accessToken, user, deviceId);
    matrix.start(true);
  }, []);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ChatList"
        component={ChatListScreen}
        options={{title: 'Chats'}}
      />
      <Stack.Screen
        name="ChatRoom"
        component={ChatScreen}
        options={({route, navigation}) => ({
          title: route.params.room.name$.getValue() || 'Chat',
          headerRight: () => (
            <Pressable
              style={({pressed}) => ({
                marginRight: 6,
                opacity: pressed ? 0.5 : 1,
              })}
              onPress={() =>
                navigation.navigate('ChatDetails', {room: route.params.room})
              }>
              <Text style={{fontSize: 30, padding: 6}}>◎</Text>
            </Pressable>
          ),
        })}
      />
      <Stack.Screen
        name="ChatDetails"
        component={ChatDetailsScreen}
        options={({route}) => ({
          title: 'Chat Details',
        })}
      />
    </Stack.Navigator>
  );
}

export default Navigator;
