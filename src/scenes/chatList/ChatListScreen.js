import React from 'react';
import {View, Text} from 'react-native';
import {RoomList} from 'rn-matrix';

export default function ChatListScreen({navigation}) {
  const navToRoom = (room) => {
    navigation.navigate('ChatRoom', {room});
  };

  return <RoomList onRowPress={navToRoom} />;
}
