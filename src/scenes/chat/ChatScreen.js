import React, {useState} from 'react';
import {View, Text, StatusBar, Pressable} from 'react-native';
import {matrix, MessageList} from 'rn-matrix';
import ActionSheet from '../../components/ActionSheet';
import {useHeaderHeight} from '@react-navigation/stack';

export default function ChatScreen({route}) {
  const [actionSheetVisible, setActionSheetVisible] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [isReplying, setIsReplying] = useState(false);
  const headerHeight = useHeaderHeight();

  const handleLongPress = (message) => {
    setActionSheetVisible(true);
    setSelectedMessage(message);
  };

  const editMessage = () => {
    setActionSheetVisible(false);
    setIsEditing(true);
  };

  const onEndEdit = () => {
    setIsEditing(false);
    setSelectedMessage(null);
  };

  const onSwipe = (message) => {
    setIsReplying(true);
    setSelectedMessage(message);
  };

  const onCancelReply = () => {
    setIsReplying(false);
    setSelectedMessage(null);
  };

  return (
    <>
      <MessageList
        room={route.params.room}
        keyboardOffset={headerHeight + StatusBar.currentHeight}
        showReactions
        enableComposer
        enableReplies
        isEditing={isEditing}
        onEndEdit={onEndEdit}
        onSwipe={onSwipe}
        isReplying={isReplying}
        onCancelReply={onCancelReply}
        selectedMessage={selectedMessage}
        onLongPress={handleLongPress}
      />
      <ActionSheet
        visible={actionSheetVisible}
        gestureEnabled={false}
        innerScrollEnabled={false}
        style={{minHeight: 100, padding: 24, paddingBottom: 48}}
        onClose={() => setActionSheetVisible(false)}>
        <Pressable
          style={({pressed}) => ({
            padding: 12,
            backgroundColor: pressed ? 'lightgray' : 'transparent',
            borderRadius: 8,
          })}
          onPress={() => {
            setIsReplying(true);
            setActionSheetVisible(false);
          }}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>Reply</Text>
        </Pressable>
        <Pressable
          style={({pressed}) => ({
            padding: 12,
            backgroundColor: pressed ? 'lightgray' : 'transparent',
            borderRadius: 8,
          })}
          onPress={editMessage}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>Edit Message</Text>
        </Pressable>
      </ActionSheet>
    </>
  );
}
