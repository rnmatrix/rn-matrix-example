import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import {useObservableState} from 'observable-hooks';
import ImagePicker from 'react-native-image-picker';
import {matrix} from 'rn-matrix';

export default function ChatDetailsScreen({route}) {
  const {room} = route.params;

  console.log(room);

  const name = useObservableState(room.name$);
  const avatar = useObservableState(room.avatar$);

  console.log('avatar', avatar);

  const setAvatar = () => {
    ImagePicker.showImagePicker(
      {mediaType: 'photo', title: 'Choose Avatar'},
      async (response) => {
        await room.setAvatar(response);
      },
    );
  };

  return (
    <View style={{alignItems: 'center', paddingTop: 48}}>
      <Pressable
        onPress={setAvatar}
        style={({pressed}) => ({opacity: pressed ? 0.5 : 1})}>
        {avatar ? (
          <View
            style={{
              backgroundColor: '#666',
              borderRadius: 100,
              overflow: 'hidden',
            }}>
            <Image
              source={{uri: matrix.getImageUrl(avatar, 125, 125)}}
              style={{width: 125, height: 125}}
            />
          </View>
        ) : (
          <View
            style={{
              backgroundColor: '#666',
              borderRadius: 100,
              width: 125,
              height: 125,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#fff', fontSize: 40, fontWeight: 'bold'}}>
              {name?.slice(0, 1)}
            </Text>
          </View>
        )}
      </Pressable>
    </View>
  );
}
